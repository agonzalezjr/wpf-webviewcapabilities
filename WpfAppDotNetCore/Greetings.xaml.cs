﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Windows.Foundation;
using Windows.Security.Cryptography;
using Windows.Storage;
using Windows.Storage.Streams;
using Microsoft.Toolkit.Win32.UI.Controls;
using Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT;

namespace WpfAppDotNetCore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void hideAll()
        {
            MyWebView.Visibility = Visibility.Hidden;
            MyWebBrowser.Visibility = Visibility.Hidden;
        }

        private void WebBrowser_Button_Click(object sender, RoutedEventArgs e)
        {
            hideAll();
            MyWebBrowser.Visibility = Visibility.Visible;
            string curDir = Directory.GetCurrentDirectory();
            MyWebBrowser.Source = new Uri(String.Format("file:///{0}/dist/index.html", curDir));
        }

        private void WebView_Button_Click(object sender, RoutedEventArgs e)
        {
            hideAll();

            log($"jsEnabled = {MyWebView.Settings.IsJavaScriptEnabled}"); // default True
            log($"scriptNotifyAllowed = {MyWebView.Settings.IsScriptNotifyAllowed}"); // default False - need to flip it!
            MyWebView.Settings.IsScriptNotifyAllowed = true;

            MyWebView.Visibility = Visibility.Visible;
            
            string curDir = Directory.GetCurrentDirectory();

            Uri fileUri = new Uri(String.Format("file:///{0}/dist/index.html", curDir));

            // Does not support navigatiom to local file:// resources
            // Both of these just throw exceptions
            // -------------------------------
            /*
            MyWebView.Source = new Uri(String.Format("file:///{0}/dist/index.html", curDir));
            */
            /*
            try
            {
                MyWebView.Navigate(uri);
            } catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.ToString());
            }
            */

            // This doesn't work in WPF either
            // -------------------------------
            // Uri msUri = new Uri(String.Format("ms-appx-web:///{0}/dist/scriptNotify_example.html", curDir));
            // Uri msUri = new Uri(String.Format("ms-appx-web:///dist/scriptNotify_example.html", curDir));
            // MyWebView.Navigate(msUri);

            // This doesn't work in WPF either
            // -------------------------------
            /*
            // Uri msUri = new Uri(String.Format("ms-appx-web:///{0}/dist/scriptNotify_example.html", curDir)); // nope
            // Uri msUri = new Uri(String.Format("ms-appx-web:///dist/scriptNotify_example.html")); // nope
            // Uri msUri = new Uri(String.Format("ms-appx:///dist/scriptNotify_example.html")); // nope (exception)
            // Uri msUri = new Uri(String.Format("ms-appx:///{0}/dist/scriptNotify_example.html", curDir)); // nope (exception)
            // Uri msUri = new Uri(String.Format("ms-appdata:///dist/scriptNotify_example.html")); // nope (exception)
            // Uri msUri = new Uri(String.Format("ms-appdata:///{0}/dist/scriptNotify_example.html", curDir)); // nope (exception)
            MyWebView.Navigate(msUri);
            */

            // This "works", but it doesn't load references very well (scripts, css, etc...)
            // -----------------------------------------------------------------------------
            /*
            string text = System.IO.File.ReadAllText("dist/index.html");
            MyWebView.NavigateToString(text);
            */

            // This doesn't work unless we setup a package identity which seems like something for UWP apps only
            // -------------------------------------------------------------------------------------------------
            /*
            StorageFolder stateFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync("NavigateToState", CreationCollisionOption.OpenIfExists);
            StorageFile htmlFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri($"ms-appx:///dist/scriptNotify_example.html"));

            await htmlFile.CopyAsync(stateFolder, "test.html", NameCollisionOption.ReplaceExisting);

            string url = "ms-appdata:///local/NavigateToState/test.html";
            MyWebView.Navigate(new Uri(url));
            */

            // var source = "dist/scriptNotify_example.html";
            var source = "dist/index.html";
            if (Uri.TryCreate(source, UriKind.Relative, out Uri result))
            {
                MyWebView.NavigateToLocalStreamUri(result, new UriToStreamResolver());
            }
            else
            {
                log("Can't create Uri");
            }
        }

        private void MyWebView_NavigationCompleted(object sender, Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT.WebViewControlNavigationCompletedEventArgs e)
        {
            log($"Navigation Completed: Success = {e.IsSuccess}; Status = \"{e.WebErrorStatus}\"");

            invokeScript();
        }

        private void MyWebView_UnviewableContentIdentified(object sender, Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT.WebViewControlUnviewableContentIdentifiedEventArgs e)
        {
            log("Unviewable Content Identified: " + e.ToString());
        }

        private void MyWebView_UnsupportedUriSchemeIdentified(object sender, Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT.WebViewControlUnsupportedUriSchemeIdentifiedEventArgs e)
        {
            log("Unsupported Uri Scheme Identified: " + e.ToString());
        }

        private void MyWebView_UnsafeContentWarningDisplaying(object sender, object e)
        {
            log("Unsafe Content Warning Displaying: " + e.ToString());
        }

        private void MyWebView_LongRunningScriptDetected(object sender, Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT.WebViewControlLongRunningScriptDetectedEventArgs e)
        {
            log("Long Running Script Detected: " + e.ToString());
        }

        private async void invokeScript()
        {
            var definition = System.IO.File.ReadAllText("dist/test-definition.json");
            var method = "launchIt";
            try
            {
                string result = await MyWebView.InvokeScriptAsync(method, definition);
                log($"Script returned \"{result}\"");
            }
            catch (Exception ex)
            {
                string errorText = null;
                switch (ex.HResult)
                {
                    case unchecked((int)0x80020006):
                        errorText = "There is no function called " + method;
                        break;
                    case unchecked((int)0x80020101):
                        errorText = "A JavaScript error or exception occured while executing the function " + method;
                        break;

                    case unchecked((int)0x800a138a):
                        errorText = method + " is not a function";
                        break;
                    default:
                        // Some other error occurred.
                        errorText = ex.Message;
                        break;
                }
                log("Invoke Script Error: " + errorText);
            }
        }

        private void log(string msg)
        {
            System.Diagnostics.Trace.WriteLine(">>> " + msg);
        }

        private void MyWebView_ScriptNotify(object sender, WebViewControlScriptNotifyEventArgs e)
        {
            log($"Event received from {e.Uri} with value: \"{e.Value}\"");
        }

        public sealed class UriToStreamResolver : IUriToStreamResolver
        {
            public Stream UriToStream(Uri uri)
            {
                System.Diagnostics.Trace.WriteLine("[u2s] UriToStream: uri = " + uri.ToString());

                string contents = "";
                if (uri.ToString().EndsWith("index.html"))
                {
                    contents = System.IO.File.ReadAllText("dist/index.html");
                }
                else if (uri.ToString().EndsWith("simpleBundle.js")) 
                {
                    contents = System.IO.File.ReadAllText("dist/simpleBundle.js");
                }
                else if (uri.ToString().EndsWith("bundle.js"))
                {
                    contents = System.IO.File.ReadAllText("dist/bundle.js");
                }
                else if (uri.ToString().EndsWith("launcher.js"))
                {
                    contents = System.IO.File.ReadAllText("dist/launcher.js");
                }
                else if (uri.ToString().EndsWith("scriptNotify_example.html"))
                {
                    contents = System.IO.File.ReadAllText("dist/scriptNotify_example.html");
                }

                var stream = new MemoryStream();
                var writer = new StreamWriter(stream);
                writer.Write(contents);
                writer.Flush();
                stream.Position = 0;

                System.Diagnostics.Trace.WriteLine("[u2s] Size of stream: uri = " + stream.Length);
                
                return stream;
            }
        }
    }
}

