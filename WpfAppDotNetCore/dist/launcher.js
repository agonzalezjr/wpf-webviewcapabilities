﻿function launchIt(p1) {
    if (!MirataRenderer) {
        return "no MR";
    }
    if (!MirataRenderer.loadForm) {
        return "no MR.lF()";
    }
    MirataRenderer.loadForm(p1);
    return "done!";
}

function notifyIt(data) {
    window.external.notify(data);
}
