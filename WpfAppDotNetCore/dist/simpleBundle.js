﻿function myAlert(msg) {
    alert(msg);
}

function aNumber() {
    return 42;
}

function aString() {
    return "Foo";
}

function anObject() {
    return JSON.stringify({
        "foo": 42
    });
}
